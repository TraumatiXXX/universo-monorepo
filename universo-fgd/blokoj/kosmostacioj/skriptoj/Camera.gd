extends Camera3D

var observoj #объект наблюдения

var _mouse_offset = Vector2()
var sensitivity = 0.5
var smoothness = 0.5
var _yaw = -15
var _pitch = 10

var distance =25.0
var distance_smooth =25.0


# warning-ignore:unused_argument
func _process(delta):
	distance_smooth =lerp(distance_smooth,distance,0.1)
	set_position(observoj.get_position())
	translate_object_local(Vector3(0.0, 0.0, distance_smooth))

	var offset = Vector2();
	offset += _mouse_offset * sensitivity
	_mouse_offset = Vector2()
	_yaw = _yaw * smoothness + offset.x * (1.0 - smoothness)
	_pitch = _pitch * smoothness + offset.y * (1.0 - smoothness)
	rotate_y(deg_to_rad(-_yaw))
	rotate_object_local(Vector3(1,0,0), deg_to_rad(-_pitch))
	pass


func doni_observoj(observo):
	observoj = observo

#func _input(event):
func _unhandled_input(event):
	if Input.get_action_strength("right_click"):
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		if event is InputEventMouseMotion:
			_mouse_offset = event.relative
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
