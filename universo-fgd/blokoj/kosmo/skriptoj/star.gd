extends Node3D
var iTime = 0.0
var cam
var mega_sun = false
func _ready():
	cam = get_tree().get_root().get_camera_3d() # получаем камеру
	$Env.environment.background_sky.set_panorama($space_viewport.get_texture())
	
func _process(delta):
	iTime +=delta
	$space_viewport/space_texture.material.set("shader_param/iTime",iTime)
	_set_sun_position()
	
func _set_sun_position():
	var position = cam.get_position()
	if mega_sun:
		$space_viewport/space_texture.material.set("shader_param/sun_radius",clamp((4000-position.distance_to(Vector3.ZERO))*0.025,0.1,100))
	position = position.normalized()
	#position.x *= -1
	position.y *= -1
	position.z *= -1
	$space_viewport/space_texture.material.set("shader_param/SUN_POS",position)
	position.x *= -1
	$star.look_at_from_position(position,Vector3.ZERO,Vector3.UP);


func _on_CheckBox_toggled(button_pressed):
	mega_sun = button_pressed

func _on_HScrollBar_value_changed(value):
	$space_viewport/space_texture.material.set("shader_param/noise_freq",value)
	print(value)
