extends Control


const QueryObject = preload("res://blokoj/rajtigo/skriptoj/queries.gd")

# Объект HTTP запроса
var request
var backend_headers = ["Content-Type: application/json"]

func _ready():
	if Global.logs:
		print('запуск res://blokoj/rajtigo/rajtigo.tscn')
	backend_headers.append("Referer: "+Net.http+"://"+Net.url_server+"/api/v1.1/")
	StartAktualaVersio()


func load_config():
	# считываем файл настроек и проверяем - может запущен серверный клиент
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err == OK:
		if Global.autoload:
			var password = config.get_value("aliro", "password")
			var login = config.get_value("aliro", "uzanto")
			$"Start_page_menuo/CanvasLayer/UI/Popup/for_auth/VBox/body_texture/LoginButton".eniri_uzanto(login, password)
		elif Global.server:
			$"Start_page_menuo/CanvasLayer/UI/Popup/for_auth/VBox/body_texture/LoginButton".eniri_server()


# Вызывается при открытии сцены
func _enter_tree():
	# Прячем глобальную сцену с меню
	Title.set_visible(false)
	
	# Скрываем символы в поле для ввода пароля
	$"Start_page_menuo/CanvasLayer/UI/Popup/for_auth/VBox/body_texture/your_password".set_secret(true)


# Вызывается при закрытии сцены
func _exit_tree():
	# Показываем глобальную сцену с меню
	Title.set_visible(true)


# проверяем связь и запрашиваем актуальную версию программы
func AktualaVersio(i):
	var URL_DATA = Net.http+"://"+Net.url_server+"/api/v1.1/"
	# Ответ будет обрабатываться в функции aktuala_request_complete
	# Данные для запросов и сами запросы храним в queries.gd
	var q = QueryObject.new()
	if not i:
		URL_DATA = q.get_URL_AUTH()
		#TODO: Refactor
	#var error = request.request(URL_DATA, backend_headers, true, i, q.get_aktuala_versio())
	# Если запрос не выполнен из-за какой-то ожибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	# TODO: Refactor
	#if error != OK:
		#print('Error in Auth Request.')


func StartAktualaVersio():
	# Создаём объект HTTP запроса
	request = HTTPRequest.new()
	# Добавляем объект запроса в сцену
	add_child(request)
	# Регистрируем обработчик сигнала request_completed, который придёт по завершении запроса
	request.connect('request_completed', Callable(self, 'start_aktuala_request_complete'))
	# Делаем запрос к бэкэнду
	AktualaVersio(0)


# Вызывается при завершении обработки запроса авторизации к бэкэнду
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func start_aktuala_request_complete(result, response_code, headers, body):
	# Вытаксиваем куки из заголовков ответа
	var cookies = PackedStringArray()
	for h in headers:
		if h.to_lower().begins_with('set-cookie'):
			cookies.append(h.split(':', true, 1)[1].strip_edges().split("; ")[0])
			var perem = h.split(':')[1].split(';')
			if perem[0].split('=')[0] == ' csrftoken':
				backend_headers.append("X-CSRFToken: %s" % perem[0].split('=')[1])
	# Куки так же сохраняем в заголовках для последующих запросов
	backend_headers.append("Cookie: %s" % "; ".join(cookies))
	request.disconnect('request_completed', Callable(self, 'start_aktuala_request_complete'))
	request.connect('request_completed', Callable(self, 'aktuala_request_complete'))
	AktualaVersio(2)


# Вызывается при завершении обработки запроса авторизации к бэкэнду
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func aktuala_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var test_json_conv = JSON.new()
	test_json_conv.parse(resp)
	var parsed_resp = test_json_conv.get_data()
	if not parsed_resp:
		print('=== нет ответа =')
		return
	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		# если версии расходятся
#		if not(parsed_resp['data']['universoAplikoVersio']['edges'].front()['node']['numeroVersio'] == Global.numero_versio and\
#				parsed_resp['data']['universoAplikoVersio']['edges'].front()['node']['numeroSubversio'] == Global.numero_subversio and\
#				parsed_resp['data']['universoAplikoVersio']['edges'].front()['node']['numeroKorektado'] == Global.numero_korektado):
#			print('Версия устарела, обновитесь на www')
#			$AktualaVersio.popup_centered()
#		else:
			load_config()
	# Если ответ от бэкэнда не содержит данные, которые мы ожидаем, выводим всё тело ответа
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	else:
		print(resp)


func _on_link_popup_hide():
	get_tree().quit()
