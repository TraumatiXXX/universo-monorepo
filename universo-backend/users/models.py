from django.db import models

from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    country = models.CharField(verbose_name='country', null=True, blank=True, max_length=255)
    midName = models.CharField(verbose_name='midName', null=True, blank=True, max_length=255)
    lastname = models.CharField(verbose_name='lastname', null=True, blank=True, max_length=255)
    firstname = models.CharField(verbose_name='firstname', null=True, blank=True, max_length=255)
    nickName = models.CharField(verbose_name='nickName', null=True, blank=True, max_length=255)
    
    UUID = models.BigIntegerField(verbose_name='UUID', null=True, blank=True)
