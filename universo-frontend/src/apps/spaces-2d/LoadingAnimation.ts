import { Router } from 'vue-router';
import { COLOR_BACKGROUND } from '../kanbans/Const';

export declare const window: {
  router: Router;
} & Window;
//Создаем анимацию загрузки канваса
export class LoadingAnimation extends Phaser.Scene {
  private sprites: Phaser.GameObjects.Sprite[] = [];
  private currentIndex: number = 0;

  constructor() {
    super({
      key: 'loading-animation',
    });
  }

  preload() {
    //Фоновый цвет
    this.cameras.main.setBackgroundColor(COLOR_BACKGROUND);
    //Спрайты
    this.load.image('sprite1', 'sprite/Sp1.png');
    this.load.image('sprite2', 'sprite/Sp2.png');
    this.load.image('sprite3', 'sprite/Sp3.png');
    this.load.image('sprite4', 'sprite/Sp4.png');
    this.load.image('sprite5', 'sprite/Sp5.png');
    this.load.image('sprite6', 'sprite/Sp6.png');
    this.load.image('sprite7', 'sprite/Sp7.png');
  }

  create() {
    //Используем закрытие
    const scene = this;
    //@ts-ignore
    this.onClose = function (onComplete) {
      scene.tweens.add({
        targets: this.sprites[0],
        scale: 0,
        duration: 1000,
        onComplete,
      });
    };
    // Создание спрайтов и добавление их в сцену
    this.sprites = Array.from({ length: 7 }, (_, i) =>
      this.add
        .sprite(window.innerWidth / 2, window.innerHeight / 2, `sprite${i + 1}`)
        .setVisible(false)
        .setScale(0.4),
    );
    // Запуск циклической смены спрайтов
    this.nextSprite();
  }

  nextSprite() {
    const currentSprite = this.sprites[this.currentIndex];
    const nextIndex = (this.currentIndex + 2) % this.sprites.length;
    const nextSprite = this.sprites[nextIndex];

    // Анимация появления следующего спрайта
    this.tweens.add({
      targets: currentSprite,
      scale: 0.4,
      //scaleX: { start: 0, to: 1 },
      //scaleY: { start: 0, to: 1 },
      duration: 100,
      onComplete: () => {
        currentSprite.setVisible(false);
        nextSprite.setVisible(true);
        this.tweens.add({
          targets: nextSprite,
          duration: 200,
          onComplete: () => {
            // Переход к следующему спрайту после завершения анимации
            this.currentIndex = nextIndex;
            this.nextSprite();
          },
        });
      },
    });
  }
}
