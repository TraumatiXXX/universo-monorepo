import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import CreateTextObject from './CreateTextObject';

const GetMaxTextObjectSize = (
  scene: EndlessCanvas,
  contentArray: string[],
  fontSize?: string,
): { width: number; height: number } => {
  const textObject = CreateTextObject(
    scene,
    '',
    fontSize,
  ) as unknown as Phaser.GameObjects.Text;
  let width = 0;
  let height = 0;
  for (const content of contentArray) {
    textObject.text = content;
    width = Math.max(textObject.width, width);
    height = Math.max(textObject.height, height);
  }
  textObject.destroy();
  return { width, height };
};

export default GetMaxTextObjectSize;
