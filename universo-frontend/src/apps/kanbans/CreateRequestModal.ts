import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { getAllChildrens } from '../utils/GetAllChildrens';
import { COLOR_BLACK, COLOR_WHITE } from './Const';
import {
  COLOR_LIGHT,
  COLOR_PRIMARY,
  COLOR_DARK,
  FONT_ORDINARY,
  FONT_LARGE,
} from './Const';

const CreateRequestModal = (
  scene: EndlessCanvas,
  callback?,
  defaultContentText: string = '',
  titleText: string = 'Редактирование объекта',
  cancelButtonText: string = 'Отменить',
  confirmButtonText: string = 'Сохранить',
) => {
  let textArea;
  if (defaultContentText !== null) {
    textArea = scene.rexUI.add
      .textAreaInput({
        x: 0,
        y: 0,
        width: 300, // Увеличена ширина
        height: 20,

        background: scene.rexUI.add.roundRectangle(
          0,
          0,
          20,
          20,
          0,
          COLOR_PRIMARY,
        ),

        text: {
          background: {
            stroke: COLOR_BLACK,
          },

          style: {
            fontSize: FONT_ORDINARY,
            //@ts-ignore
            backgroundBottomY: 1,
            backgroundHeight: 20,
            cursor: {
              color: COLOR_BLACK,
              backgroundColor: COLOR_WHITE,
            },
            color: COLOR_WHITE,
          },
        },

        space: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          text: 10,
          header: 0,
          footer: 0,
        },

        mouseWheelScroller: {
          focus: false,
          speed: 0.1,
        },

        content: defaultContentText,
      })
      .layout()
      .on('textchange', (newText: string) => {
        callback(newText);
      });

    textArea.setDepth(2);
  }
  const cancelButton = scene.rexUI.add
    .label({
      background: scene.rexUI.add.roundRectangle(0, 0, 0, 0, 20, COLOR_DARK),
      text: scene.add.text(0, 0, cancelButtonText, { fontSize: FONT_LARGE }),
      space: { left: 10, right: 10, top: 10, bottom: 10 },
    })
    .setDepth(3);

  const confirmButton = scene.rexUI.add
    .label({
      background: scene.rexUI.add.roundRectangle(0, 0, 0, 0, 20, COLOR_LIGHT),
      text: scene.add.text(0, 0, confirmButtonText, { fontSize: FONT_LARGE }),
      space: { left: 10, right: 10, top: 10, bottom: 10 },
    })

    .setDepth(3);

  const dialog = scene.rexUI.add
    .dialog({
      background: scene.rexUI.add
        .roundRectangle(0, 0, 0, 0, 20, COLOR_WHITE)
        .setStrokeStyle(2, COLOR_BLACK),

      title: scene.rexUI.add.label({
        text: scene.add.text(0, 0, titleText, {
          fontSize: FONT_LARGE,
          color: COLOR_BLACK.toString(),
        }),
        space: {
          left: 20, // Увеличены отступы
          right: 20,
          top: 15,
          bottom: 15,
        },
      }),

      content: textArea,

      actions: [cancelButton, confirmButton],

      space: {
        title: 30, // Увеличены отступы
        content: 30,
        action: 20,

        left: 25,
        right: 25,
        top: 25,
        bottom: 25,
      },

      align: {
        actions: 'right',
      },

      expand: {
        content: false,
      },
    })
    .on('button.over', (button, groupName, index, pointer, event) => {
      button.getElement('background').setStrokeStyle(1, COLOR_BLACK);
    })
    .on('button.out', (button, groupName, index, pointer, event) => {
      button.getElement('background').setStrokeStyle();
    });
  dialog.layout();

  scene.cameras.main.ignore(getAllChildrens(dialog));

  return dialog;
};
export default CreateRequestModal;
